Code Examples
=============

**mvcexample** Very basic setup of Struts 2 and Spring 3, with a basic form and response.  Maven build included

**logtoemail** A simple demonstration of routing in Spring Integration
