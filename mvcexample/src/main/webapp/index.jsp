<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
</head>
<body>
<s:form action="formResult" validate="true">
		<s:textfield name="person.title" label="Title"/>
		<s:textfield name="person.firstName" label="First Name"/>
		<s:textfield name="person.surName" label="Surname"/>
		<s:textfield name="person.address"  label="Address"/>
		<s:textfield name="person.ageInYears"  label="Age In Years"/>
		
        <s:submit value="Save" align="left" />
</s:form>
</body>
</html>