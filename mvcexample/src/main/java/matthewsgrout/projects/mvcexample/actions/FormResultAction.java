package matthewsgrout.projects.mvcexample.actions;

import matthewsgrout.projects.mvcexample.beans.Person;

public class FormResultAction extends AbstractStrutsAction {

	
	private static final long serialVersionUID = 1753791852342849057L;
	private Person person;
	
	@Override
	public String execute() throws Exception {
		if (person.getAgeInYears()>0) person.setAgeInDays(person.getAgeInYears()*365);
		// Do something else here with person like validate or save to database
		return SUCCESS;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
