package matthewsgrout.projects.mvcexample.actions;

import com.opensymphony.xwork2.ActionSupport;

public abstract class AbstractStrutsAction extends ActionSupport {

	private static final long serialVersionUID = 3588917900278628869L;

	public abstract String execute() throws Exception;

}
