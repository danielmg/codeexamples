package danielmg.stuff.stacktraces.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import danielmg.stuff.stacktraces.dao.BookingRepository;
import danielmg.stuff.stacktraces.dao.CarRepository;
import danielmg.stuff.stacktraces.data.Booking;
import danielmg.stuff.stacktraces.data.Booking.BookingType;
import danielmg.stuff.stacktraces.data.Car;
import danielmg.stuff.stacktraces.exceptions.InvalidBookingDateException;
import danielmg.stuff.stacktraces.service.GarageService;

@Service
public class GarageServiceImpl implements GarageService {

	@Autowired
	private CarRepository carRepository;

	@Autowired
	private BookingRepository bookingRepository;

	@Override
	public List<Car> getCarsByOwner(String ownerName) {
		return this.carRepository.findByOwnerName(ownerName);
	}

	@Override
	public Integer saveCar(Car car) {
		return this.carRepository.saveAndFlush(car).getId();
	}

	@Override
	public Integer bookService(Car car, Date date) throws InvalidBookingDateException {
		if (date.getTime() < Calendar.getInstance().getTimeInMillis())
			throw new InvalidBookingDateException("Date is in the past: " + date);
		return this.bookingRepository.saveAndFlush(new Booking(car, date, BookingType.Service)).getId();
	}

	@Override
	public void deleteCar(Integer carId) {
		this.carRepository.delete(carId);
	}

	@Override
	public void cancelBooking(Integer bookingId) {
		this.bookingRepository.delete(bookingId);
	}

	public void setCarRepository(CarRepository carRepository) {
		this.carRepository = carRepository;
	}

	public void setBookingRepository(BookingRepository bookingRepository) {
		this.bookingRepository = bookingRepository;
	}

}
