package danielmg.stuff.stacktraces.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import danielmg.stuff.stacktraces.data.Booking;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {
}
