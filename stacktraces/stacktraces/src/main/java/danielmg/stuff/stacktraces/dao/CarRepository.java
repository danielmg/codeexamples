package danielmg.stuff.stacktraces.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import danielmg.stuff.stacktraces.data.Car;

@Repository
public interface CarRepository extends JpaRepository<Car, Integer> {
	public List<Car> findByOwnerName(String ownerName);
}
