package danielmg.stuff.stacktraces;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import danielmg.stuff.stacktraces.data.Car;
import danielmg.stuff.stacktraces.exceptions.GarageException;
import danielmg.stuff.stacktraces.exceptions.InvalidBookingDateException;
import danielmg.stuff.stacktraces.service.GarageService;

@ComponentScan
@Service
public class App {
	@Autowired
	private GarageService garageService;

	public static void main(String[] args) throws GarageException, InterruptedException {
		@SuppressWarnings("resource")
		App app = (App) new ClassPathXmlApplicationContext("applicationContext.xml").getBean(App.class);
		app.start();
	}

	public void start() throws GarageException, InterruptedException {
		
		System.out.println("");
		System.out.println("** Garage Application Started **");
		System.out.println("");

		
		Car car = new Car();
		car.setOwnerName("dan");

		System.out.println("Car id added: " + this.garageService.saveCar(car));

		Calendar cal = Calendar.getInstance();

		cal.add(Calendar.DAY_OF_YEAR, -1);

		
		System.out.println("");
		System.out.println("Forcing an unchecked Null Pointer Exception and catching it explicitly");
		System.out.println("");

		// force unchecked NPE
		try {
			this.garageService.bookService(car, null);
		} catch (InvalidBookingDateException | NullPointerException e) {
			e.printStackTrace();
		}
		
		//for some reason print stack trace happens on a different thread and races with out.println
		Thread.sleep(100);
		
		System.out.println("");
		System.out.println("Forcing an unchecked exception from 3rd party framework and catching it explicitly");
		System.out.println("");

		try {
			this.garageService.deleteCar(car.getId() + 1);
		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
		
		//for some reason print stack trace happens on a different thread and races with out.println
		Thread.sleep(100);
		
		System.out.println("");
		System.out.println("Forcing a checked exception, wrapping it into another checked exception and then throwing that");
		System.out.println("");

		// force checked exception and wrap and throw
		try {
			this.garageService.bookService(car, cal.getTime());
		} catch (InvalidBookingDateException e) {
			throw new GarageException(e);
		}
	}

	public void setGarageService(GarageService garageService) {
		this.garageService = garageService;
	}

}
