package danielmg.stuff.stacktraces.service;

import java.util.Date;
import java.util.List;

import danielmg.stuff.stacktraces.data.Car;
import danielmg.stuff.stacktraces.exceptions.InvalidBookingDateException;

public interface GarageService {
	
	public List<Car> getCarsByOwner(String ownerName);
	public Integer saveCar(Car car);
	public Integer bookService(Car car, Date date) throws InvalidBookingDateException;
	public void deleteCar(Integer carId);
	public void cancelBooking(Integer bookingId);
}
