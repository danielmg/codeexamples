package danielmg.stuff.stacktraces.exceptions;

public class GarageException extends Exception {

	private static final long serialVersionUID = 11009259221334741L;

	public GarageException(Throwable t) {
		super(t);
	}

}
