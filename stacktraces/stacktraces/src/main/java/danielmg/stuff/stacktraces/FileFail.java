package danielmg.stuff.stacktraces;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


public class FileFail {

	public static void main(String[] args)  {
	
		try {
			new FileFail().doStuff();
		} catch (AppException e) {
			throw new DeadAppRTE(e);
		}
		
	}
	
	public void doStuff() throws AppException {
		try {
		this.openFile();
		} catch (DanException e) {
			throw new AppException(e);
		}
	}
	
	private void openFile() throws DanException {
		
		try {
		File file = new File("c:\\fake\\directory\\fake.file");
		
		InputStream is = new FileInputStream(file);
		int b;
		while ((b = is.read())>0) {
			System.out.print((byte)b);
		}
		is.close(); } catch (IOException e) {
			throw new DanException(e);
		}
		
	}

	
	class DanException extends Exception {
		
		private static final long serialVersionUID = -4952471523675148937L;

		public DanException(Throwable t) {
			super(t);
		}
	}

	class AppException extends Exception {
		
		private static final long serialVersionUID = 3599480991698587516L;

		public AppException(Throwable t) {
			super(t);
		}
	}

	static class DeadAppRTE extends RuntimeException {

		private static final long serialVersionUID = 1065482519961632763L;

		public DeadAppRTE(Throwable t) {
			super(t);
		}
		
	}
	
}
