package danielmg.stuff.stacktraces.exceptions;

public class InvalidBookingDateException extends Exception {

	private static final long serialVersionUID = 4206556456737769066L;

	public InvalidBookingDateException(String msg) {
		super(msg);
	}

}
