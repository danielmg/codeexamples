package danielmg.stuff.stacktraces.data;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Booking {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@ManyToOne
	private Car car;
	
	public enum BookingType{Service,Valet,MOT}
	
	private Date date;
	
	private BookingType bookingType;
	
	public Booking(){}
	public Booking(Car car, Date date, BookingType bookingType) {
		super();
		this.car = car;
		this.date = date;
		this.bookingType=bookingType;
	}

	
	
	public BookingType getBookingType() {
		return bookingType;
	}
	public void setBookingType(BookingType bookingType) {
		this.bookingType = bookingType;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
