package matthewsgrout.projects.logtoemail;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
	public static void main(String[] args) {  
         new ClassPathXmlApplicationContext("logtoemail-context.xml");  
        try {  
             while(System.in.read()!='q');  
        } catch (IOException e) {  
             e.printStackTrace();  
        }  
   }  
}
